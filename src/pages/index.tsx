
import type { NextPage } from 'next'
import Home from '../views/Home'

const Page:NextPage = () => {
	return (
		<Home />
	)
}

export default Page
