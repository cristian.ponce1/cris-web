import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { createTheme, NextUIProvider } from '@nextui-org/react'
import { ThemeProvider as NextThemesProvider } from 'next-themes'
import { QueryClient, QueryClientProvider } from '@tanstack/react-query'
const queryClient = new QueryClient()
function MyApp ({ Component, pageProps }:AppProps) {
	return (
		<QueryClientProvider client={queryClient}>

			<NextThemesProvider
				defaultTheme="system"
				attribute="class"
				value={{
					light: lightTheme.className,
					dark: darkTheme.className
				}}
			>

				<NextUIProvider>
					<Component {...pageProps} />
				</NextUIProvider>
			</NextThemesProvider>
		</QueryClientProvider>
	)
}

export default MyApp

const lightTheme = createTheme({
	type: 'light',
	theme: {
	}
})

const darkTheme = createTheme({
	type: 'dark',
	theme: {
	}
})
