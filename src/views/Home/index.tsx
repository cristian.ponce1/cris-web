import Layout from 'Layout'
import { GoogleAnalytics } from 'nextjs-google-analytics'
import Footer from './components/Footer'
import Inicio from './components/Inicio'
import Portafolio from './components/Portafolio'
import Stack from './components/Stack'

const Home = () => {
	return (
		<Layout>
			<GoogleAnalytics trackPageViews />
			<Inicio />
			<Stack />
			<Portafolio />
			<Footer />
		</Layout>
	)
}

export default Home
