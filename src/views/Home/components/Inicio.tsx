import { Button, Grid, Modal, Text } from '@nextui-org/react'
import Lottie from 'lottie-react'
import React, { useState } from 'react'
import computer from '@utils/computer.json'

const Inicio = () => {
	const [visible, setVisible] = useState(false)
	const closeHandler = () => {
		setVisible(false)
	}
	return (
		<div style={{
			height: '80vh',
			display: 'flex',
			flexDirection: 'column',
			justifyContent: 'center'
		}}>
			<Modal
				closeButton
				aria-labelledby="modal-title"
				open={visible}
				onClose={closeHandler}
			>
				<Modal.Header justify='flex-start'>
					<Text size={18} style={{ fontWeight: 'bold' }}>
            Contacto
					</Text>
				</Modal.Header>
				<Modal.Body>
					<Text>Email: cristianponce94@outlook.com</Text>
				</Modal.Body>
				<Modal.Footer>
					<Button auto flat color="error" onPress={closeHandler}>
            Cancelar
					</Button>

					<a href="mailto:cristianponce94@outlook.com">
						<Button auto>
            Enviar correo
						</Button>
					</a>

				</Modal.Footer>
			</Modal>

			<Grid.Container gap={2}>
				<Grid xs={12} md={6}>
					<div style={{ display: 'flex', flexDirection: 'column' }}>

						<Text
							weight={'bold'}
							h1
							style={{ marginTop: 20 }}
						>{'Hola,  soy'}</Text>
						<div style={{ display: 'flex', marginBottom: 20 }}>

							<Text h1
								weight={'bold'}

								css={{
									textGradient: '65deg, $blue600 -20%, $pink600 50%'
								}}
							>Cristian Ponce</Text>
						</div>
						<div style={{ marginBottom: 40 }}>

							<Text>

Desarrollador de software, graduado en la Escuela Politecnica Nacional como ingeniero en
Electrónica y Control. Principalmente programo en Javascript junto con Typescript. En consecuencia mi principal entorno es Node JS y las tecnologías que de este derivan.
							</Text>
						</div>
						<Button color={'gradient'} size='lg' shadow
							style={{ maxWidth: 300, width: '100%' }}
							onClick={() => setVisible(true)}
						>Contáctame</Button>
					</div>
				</Grid>
				<Grid xs={0} md={6}>
					<div style={{
						display: 'flex',
						justifyContent: 'end',
						width: '100%'
					}}>

						<Lottie animationData={computer} />
					</div>
				</Grid>
			</Grid.Container>
		</div>
	)
}

export default Inicio
