import { Spacer } from '@nextui-org/react'

import React from 'react'
import { AiOutlineGitlab, AiFillLinkedin, AiOutlineGithub } from 'react-icons/ai'

const Footer = () => {
	return (
		<div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', paddingBottom: 40 }}>
			<a href='https://gitlab.com/cristian.ponce1' target={'_blank'} rel="noreferrer">

				<AiOutlineGitlab size={30} />
			</a>

			<Spacer />
			<a href='https://www.linkedin.com/in/cristian-ponce-032954163/' target={'_blank'} rel="noreferrer">

				<AiFillLinkedin size={30} />
			</a>
			<Spacer />
			<a href='https://github.com/CristianDavidPonce' target={'_blank'} rel="noreferrer">

				<AiOutlineGithub size={30} />
			</a>
		</div>
	)
}

export default Footer
