import { Badge, Button, Card, Grid, Text } from '@nextui-org/react'
import React, { ReactNode } from 'react'
import { FaBlog, FaEye, FaMobileAlt } from 'react-icons/fa'
import { MdHealthAndSafety, MdCameraAlt } from 'react-icons/md'
import { useMediaQuery } from 'react-responsive'

const Portafolio = () => {
	const isMobile = useMediaQuery({ maxWidth: 700 })
	return (
		<>
			<div style={{ display: 'flex', marginTop: 40 }}>

				<Text h2
					weight={'bold'}
					style={{ marginBlock: 20, display: 'flex', alignItems: 'center', flexWrap: 'wrap' }}
				> Hecha un vistazo a mi
					<Text h2
						weight={'bold'}
						style={{ marginBlock: 20, marginLeft: 10 }}
						css={{
							textGradient: '65deg, $blue600 -20%, $pink600 50%'
						}}
					>portafolio</Text>
				</Text>

			</div>
			{
				items.map((item, i) => {
					const isPar = i % 2 === 0
					return <div key={item.title}
						style={{
							display: 'flex',
							justifyContent: 'center',
							flexDirection: !isMobile ? (isPar ? 'row' : 'row-reverse') : 'column',
							alignItems: 'center',
							marginBottom: 120
						}}
					>
						<div key={item.title}>
							<Card variant='flat' style={{ background: 'transparent' }}>
								<Card.Image
									src={item.src}
									alt="Default Image"
									objectFit="cover"
									height={500}
								/>
							</Card>
						</div>
						<div style={{
							alignContent: 'center',
							alignItems: 'center',
							justifyContent: 'center',
							marginLeft: (isPar && !isMobile) ? -60 : 0,
							marginRight: (!isPar && !isMobile) ? -60 : 0,
							marginTop: isMobile ? -60 : 0
						}}>
							<Card css={{ p: '$6', maxW: 400 }} variant='shadow'>
								<Card.Header>
									{item.icon}
									<Grid.Container css={{ pl: '$6' }}>
										<Grid xs={12}>
											<Text h4 css={{ lineHeight: '$xs' }}>
												{item.title}
											</Text>
										</Grid>
										<Grid xs={12}>
											<Text css={{ color: '$accents8' }}>{item.subTitle}</Text>
										</Grid>
									</Grid.Container>
								</Card.Header>
								<Card.Body>
									<Text css={{ whiteSpace: 'pre-line' }}>
										{item.description}
									</Text>

									<div style={{ marginTop: 10 }}>
										{
											item.tags.map(tag => <Badge key={tag} variant="flat" color={'secondary'}>{tag}</Badge>)
										}
									</div>
									{ item.href && <div style={{ display: 'flex', justifyContent: 'end' }}>

										<a href={item.href} target='_blank' rel="noreferrer">
											<Button flat size={'sm'} icon={<FaEye />}>Live Preview</Button>
										</a>

									</div>}
								</Card.Body>

							</Card>
						</div>
					</div>
				})
			}

		</>
	)
}

export default Portafolio

interface IItem {
	title:string;
	src:string;
	description:string,
	tags:string[],
	icon:ReactNode,
	subTitle:string,
	href?:string
}

const items:IItem[] = [
	{
		title: 'Blog',
		subTitle: 'Blog personal full stack',
		description: 'BACKEND: Framework: NestJs, Autentificación: JWT, Autorización: Basada en roles y permisos, Base de datos: MySql, Orm: TypeOrm. FRONTEND: NextJS, Tailwind, EditorJS.',
		src: '/proyectos/blog.png',
		tags: ['NestJS', 'MySql', 'Typeorm', 'React JS', 'Next JS', 'Tailwind'],
		icon: <FaBlog size={30} />,
		href: 'https://blog.cristianponce.com/'
	},
	{
		title: 'Artem',
		subTitle: 'Sistema para administración de centros médicos y hospitales.',
		description: 'Módulos: Pacientes, Agenda, Imagen, Farmacia, Terapia, Inventario, Usuarios y Roles',
		src: '/proyectos/artem1.png',
		tags: ['Typescript', 'React JS', 'Next JS', 'Ant Design', 'MongoDb'],
		icon: <MdHealthAndSafety size={30} />

	},
	{
		title: 'Dicom Viewer',
		subTitle: 'Visualizador de imagenes DICOM.',
		description: 'Herramienta totalmente web que permite visualizar imágenes de tipo DCM. Además, posee herramientas como reglas, ángulos, brillo, contraste y zoom. ',
		src: '/proyectos/dicom1.png',
		tags: ['Typescript', 'React JS'],
		icon: <MdCameraAlt size={30} />
	},
	{
		title: 'Aplicación Móvil',
		subTitle: 'Para pacientes',
		description: 'Aplicación iOS y android, para revisión de resultados de laboratorio, imágen e historial médico. Publicada en la AppStore y PlayStore',
		src: '/proyectos/app.png',
		tags: ['Typescript', 'ReactNative', 'Expo', 'Material Design 3'],
		icon: <FaMobileAlt size={30} />
	},
	{
		title: 'RoboCop',
		subTitle: 'Programa de escritorio',
		description: 'Robocop automatiza tareas de sincronización de datos desde un programa instalado en windows hasta un sistema wed propio.',
		src: '/proyectos/bot.png',
		tags: ['Electronjs', 'Reactjs', 'GeistUI', 'Node Cron', 'Nutjs'],
		icon: <MdCameraAlt size={30} />
	},
	{
		title: 'Sistema de gestión',
		subTitle: 'Sistema de gestión de clientes de un estudio jurídico',
		description: 'Módulos: Clientes, Trámites, Casos, Usuarios y Roles',
		src: '/proyectos/abogadoweb.png',
		tags: ['Typescript', 'React JS', 'Next JS', 'Ant Design', 'MongoDb'],
		icon: <MdHealthAndSafety size={30} />
	},
	{
		title: 'Aplicación Móvil',
		subTitle: 'Para clientes de abogados',
		description: 'Aplicación iOS y android, para revisión de trámites y casos, herramientas de cálculos como jubilación patronal y liquidaciones laborales.',
		src: '/proyectos/abogado.png',
		tags: ['Typescript', 'ReactNative', 'Expo', 'Material Design 3'],
		icon: <FaMobileAlt size={30} />
	}

]
