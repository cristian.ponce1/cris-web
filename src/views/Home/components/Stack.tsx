import { Card, Grid, Text } from '@nextui-org/react'
import React, { ReactNode } from 'react'
import { FaDesktop, FaFirefoxBrowser, FaMobileAlt, FaServer } from 'react-icons/fa'
const Stack = () => {
	return (
		<Grid.Container gap={4} justify='center'>
			{
				items.map(item => <Grid key={item.title} md={6} lg={4} xs={12}>
					<Card css={{ p: '$6' }}>
						<Card.Header>

							{item.icon}
							<Grid.Container css={{ pl: '$6' }}>
								<Grid xs={12}>
									<Text css={{ color: '$accents8' }}>Aplicaciones</Text>
								</Grid>
								<Grid xs={12}>
									<Text h4 css={{ lineHeight: '$xs' }}>
										{item.title}
									</Text>
								</Grid>
							</Grid.Container>

						</Card.Header>
						<Card.Body>
							<Text>
								{item.description}
							</Text>
						</Card.Body>
					</Card>
				</Grid>)
			}

		</Grid.Container>
	)
}

export default Stack

interface ICard {
	title:string;
	description:ReactNode;
	icon:ReactNode
}

const items:ICard[] = [
	{
		title: 'FRONTEND',
		description: <>
    Para el desarrollo de aplicaciones webs me especializo en el uso de <strong> React.js </strong>
    junto a <strong> Next.js</strong>. Además de el uso de
    frameworks UI como Ant Desing, Material UI, NextUI y Geist. Para el despliegue he usado Vercel, Google Cloud y Netlify.
		</>,
		icon: <FaFirefoxBrowser size={30} />
	},
	{
		title: 'BACKEND',
		description: <>
     Si bien me especializo en frontend, he participado en proyectos BackEnd en donde utilizado las siguientes herramientas.
Frameworks para el servidor: Fastify, Restify y Nest.js. MongoDb como base de datos y para el
              despliegue Google Cloud App Engine y Railway.app
		</>,
		icon: <FaServer size={30} />
	},
	{
		title: 'MOVIL',
		description: <>
    Para el desarrollo de aplicaciones IOS y Android uso <strong> React Native </strong>
junto con el flujo de trabajo de <strong> Expo</strong>. Estas tecnologías me han ayudado ha construir aplicaciones en tiempos record y subirlas a las diferentes tiendas (App Store y Playstore)
		</>,
		icon: <FaMobileAlt size={30} />
	},
	{
		title: 'ESCRITORIO',
		description: <>
   Para el desarrollo de aplicaciones de Mac OS, Windows y Linux uso <strong> Electron JS </strong>.
		</>,
		icon: <FaDesktop size={30} />
	},
	{
		title: 'LOW CODE',
		description: <>
   Para el desarrollo de aplicaciones web backend y frontend he utilizado <strong> Caspio Bridge </strong> y para el desarrollo de páginas web y crm <strong>Webflow</strong>.
		</>,
		icon: <FaDesktop size={30} />
	}
]
