import { Navbar, Text, Switch, useTheme, Container } from '@nextui-org/react'
import React, { ReactNode } from 'react'
import { useTheme as useNextTheme } from 'next-themes'
interface IProps {
	children:ReactNode
}
const Layout = ({ children }:IProps) => {
	const { setTheme } = useNextTheme()
	const { isDark } = useTheme()
	return (
		<div>

			<Navbar variant={'floating'} shouldHideOnScroll isBordered={isDark}>
				<Navbar.Brand>
					<Navbar.Toggle aria-label="toggle navigation" />
					<Text b color="inherit" hideIn="xs">
            Cris
					</Text>
				</Navbar.Brand>
				<Navbar.Content hideIn="xs">
					<Navbar.Link isActive href="#">Inicio</Navbar.Link>
					<Navbar.Link href="#">Usuarios</Navbar.Link>
					<Navbar.Link href="#">Contacto</Navbar.Link>
				</Navbar.Content>
				<Navbar.Content>
					<Text>
          Claro o Obscuro 😐
					</Text>
					<Switch
						checked={isDark}
						onChange={(e) => setTheme(e.target.checked ? 'dark' : 'light')}
					/>
				</Navbar.Content>
				<Navbar.Content>
					<Navbar.Link color="inherit" href="#">
            Cerrar sesion
					</Navbar.Link>
				</Navbar.Content>
			</Navbar>
			<Container style={{ padding: 20 }}>

				{children}
			</Container>
		</div>
	)
}

export default Layout
