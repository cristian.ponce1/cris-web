import { Navbar, Switch, useTheme, Container, Badge, Avatar } from '@nextui-org/react'
import React, { ReactNode } from 'react'
import { useTheme as useNextTheme } from 'next-themes'
import { FaMoon, FaRegSun } from 'react-icons/fa'
interface IProps {
	children:ReactNode
}
const Layout = ({ children }:IProps) => {
	const { setTheme } = useNextTheme()
	const { isDark } = useTheme()
	return (
		<div>

			<Navbar variant={'floating'} shouldHideOnScroll isBordered={isDark}>
				<Navbar.Brand>
					<Navbar.Toggle aria-label="toggle navigation" showIn={'xs'} />
					<Avatar
						squared
						src="foto.jpg" />
				</Navbar.Brand>
				<Navbar.Content hideIn="xs">
					<Navbar.Link isActive href="#">Inicio</Navbar.Link>
					<Badge content='Beta' variant={'flat'} color='secondary' horizontalOffset={'-50%'} verticalOffset={'-20%'}>
						<Navbar.Link href="https://blog.cristianponce.com/" target={'_blank'}>Blog</Navbar.Link>
					</Badge>
				</Navbar.Content>
				<Navbar.Content>
					<Switch
						checked={isDark}
						shadow
						color={'secondary'}
						size='lg'
						onChange={(e) => setTheme(e.target.checked ? 'dark' : 'light')}
						iconOn={<FaMoon />}
						iconOff={<FaRegSun />}
					/>

				</Navbar.Content>

				<Navbar.Collapse>
					<Navbar.CollapseItem>

						Inicio
					</Navbar.CollapseItem>
					<Navbar.CollapseItem>
						Blog
					</Navbar.CollapseItem>
				</Navbar.Collapse>
			</Navbar>
			<Container>

				{children}
			</Container>
		</div>
	)
}

export default Layout
